const { app, BrowserWindow } = require('electron');
const path = require('path');

function createWindow () {
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'src/preload.js'),
      // Example CSP (customize based on your app's needs)
      // More restrictive policies are recommended for production
      // Read more: https://electronjs.org/docs/tutorial/security#6-define-a-content-security-policy
      contextIsolation: true, // This is necessary for contextBridge
      sandbox: true, // Enable sandbox mode
      webSecurity: true, // Enable web security
      enableRemoteModule: false, // Disable remote module
      nodeIntegration: true, // Disable node integration
      allowRunningInsecureContent: false, // Disable insecure content
      preload: path.join(__dirname, 'src/preload.js')
    },
  });

  mainWindow.loadFile('src/index.html');
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
