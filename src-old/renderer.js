document.getElementById('tracker-form').addEventListener('submit', async (event) => {
    event.preventDefault();
    const title = document.getElementById('title').value;
    const description = document.getElementById('description').value;
  
    // Add the new entry using the exposed API
    await window.api.addEntry({ title, description });
  
    // Update the UI
    updateEntriesList();
  
    // Clear the form
    event.target.reset();
  });
  
  function updateEntriesList() {
    const entriesList = document.getElementById('entries-list');
    entriesList.innerHTML = '';
  
    // Get entries using the exposed API
    const entries = window.api.getEntries();
    entries.forEach((entry, index) => {
      const listItem = document.createElement('li');
      listItem.textContent = `${entry.title}: ${entry.description}`;
      entriesList.appendChild(listItem);
    });
  }
  
  // Update the entries list on page load
  updateEntriesList();
  