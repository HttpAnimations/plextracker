const { contextBridge, ipcRenderer } = require('electron');
const Store = require('electron-store');
const store = new Store();

contextBridge.exposeInMainWorld('api', {
  getEntries: () => store.get('entries', []),
  addEntry: (entry) => {
    const entries = store.get('entries', []);
    entries.push(entry);
    store.set('entries', entries);
  }
});
