#include <gtk/gtk.h>
#include <json-c/json.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Structure to hold movie information
typedef struct {
    GtkWidget *name_entry;
    GtkWidget *icon_entry;
    GtkWidget *year_entry;
    GtkWidget *movies_list;
    char *json_file_path;
} MovieData;

// Function to add a movie
void add_movie(GtkWidget *widget, gpointer data) {
    MovieData *movie_data = (MovieData *)data;

    const gchar *name = gtk_entry_get_text(GTK_ENTRY(movie_data->name_entry));
    const gchar *icon = gtk_entry_get_text(GTK_ENTRY(movie_data->icon_entry));
    const gchar *year = gtk_entry_get_text(GTK_ENTRY(movie_data->year_entry));

    // Validate input (you can add more validation as needed)
    if (strlen(name) == 0 || strlen(icon) == 0 || strlen(year) == 0) {
        g_print("Please fill in all fields.\n");
        return;
    }

    // Load existing JSON file or create new JSON array
    json_object *root_object = NULL;
    json_object *json_array = NULL;

    if (movie_data->json_file_path) {
        FILE *file = fopen(movie_data->json_file_path, "r+");
        if (file == NULL) {
            g_print("Failed to open JSON file for reading.\n");
            return;
        }

        fseek(file, 0, SEEK_END);
        long fileSize = ftell(file);
        fseek(file, 0, SEEK_SET);

        char *buffer = (char *)malloc(fileSize + 1);
        size_t nread = fread(buffer, 1, fileSize, file);
        buffer[nread] = '\0';

        fclose(file);

        root_object = json_tokener_parse(buffer);
        free(buffer);

        if (root_object == NULL) {
            g_print("Failed to parse JSON file.\n");
            return;
        }

        if (!json_object_object_get_ex(root_object, "movies", &json_array)) {
            g_print("JSON file does not contain a valid 'movies' array.\n");
            json_object_put(root_object);
            return;
        }
    } else {
        root_object = json_object_new_object();
        json_array = json_object_new_array();
        json_object_object_add(root_object, "movies", json_array);
    }

    // Create JSON object for the new movie
    json_object *new_movie = json_object_new_object();
    json_object_object_add(new_movie, "title", json_object_new_string(name));
    json_object_object_add(new_movie, "icon", json_object_new_string(icon));
    json_object_object_add(new_movie, "release_date", json_object_new_string(year));

    // Add the new movie to the JSON array
    json_object_array_add(json_array, new_movie);

    // Save JSON object to file
    FILE *file = fopen(movie_data->json_file_path, "w");
    if (file == NULL) {
        g_print("Failed to open JSON file for writing.\n");
        json_object_put(root_object);
        return;
    }

    const char *json_string = json_object_to_json_string_ext(root_object, JSON_C_TO_STRING_PRETTY);
    fprintf(file, "%s\n", json_string);

    fclose(file);
    g_print("Movie added successfully.\n");

    // Clean up JSON objects
    json_object_put(new_movie);
    json_object_put(root_object);

    // Clear entry fields after adding movie
    gtk_entry_set_text(GTK_ENTRY(movie_data->name_entry), "");
    gtk_entry_set_text(GTK_ENTRY(movie_data->icon_entry), "");
    gtk_entry_set_text(GTK_ENTRY(movie_data->year_entry), "");
}

// Function to handle file chooser dialog
void choose_json_file(GtkWidget *widget, gpointer data) {
    MovieData *movie_data = (MovieData *)data;

    GtkWidget *dialog;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
    gint res;

    dialog = gtk_file_chooser_dialog_new("Open JSON File",
                                         GTK_WINDOW(gtk_widget_get_toplevel(GTK_WIDGET(widget))),
                                         action,
                                         "_Cancel",
                                         GTK_RESPONSE_CANCEL,
                                         "_Open",
                                         GTK_RESPONSE_ACCEPT,
                                         NULL);

    res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_ACCEPT) {
        char *filename;
        GtkFileChooser *chooser = GTK_FILE_CHOOSER(dialog);
        filename = gtk_file_chooser_get_filename(chooser);
        g_print("Selected file: %s\n", filename);

        // Store the selected file path in movie_data
        if (movie_data->json_file_path) {
            g_free(movie_data->json_file_path);
        }
        movie_data->json_file_path = g_strdup(filename);

        g_free(filename);
    }

    gtk_widget_destroy(dialog);
}

// Main function
int main(int argc, char *argv[]) {
    GtkWidget *window;
    GtkWidget *grid;
    GtkWidget *add_movie_button;
    GtkWidget *choose_file_button;
    MovieData *movie_data = g_malloc(sizeof(MovieData));
    movie_data->json_file_path = NULL;

    // Initialize GTK
    gtk_init(&argc, &argv);

    // Create main window
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Movie Editor");
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    // Create grid layout
    grid = gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(window), grid);

    // Create entries for movie data
    GtkWidget *name_label = gtk_label_new("Name:");
    gtk_grid_attach(GTK_GRID(grid), name_label, 0, 0, 1, 1);
    movie_data->name_entry = gtk_entry_new();
    gtk_grid_attach(GTK_GRID(grid), movie_data->name_entry, 1, 0, 1, 1);

    GtkWidget *icon_label = gtk_label_new("Icon Path:");
    gtk_grid_attach(GTK_GRID(grid), icon_label, 0, 1, 1, 1);
    movie_data->icon_entry = gtk_entry_new();
    gtk_grid_attach(GTK_GRID(grid), movie_data->icon_entry, 1, 1, 1, 1);

    GtkWidget *year_label = gtk_label_new("Year:");
    gtk_grid_attach(GTK_GRID(grid), year_label, 0, 2, 1, 1);
    movie_data->year_entry = gtk_entry_new();
    gtk_grid_attach(GTK_GRID(grid), movie_data->year_entry, 1, 2, 1, 1);

    // Create button to add movie
    add_movie_button = gtk_button_new_with_label("Add Movie");
    gtk_grid_attach(GTK_GRID(grid), add_movie_button, 0, 3, 2, 1);
    g_signal_connect(add_movie_button, "clicked", G_CALLBACK(add_movie), movie_data);

    // Create button to choose JSON file
    choose_file_button = gtk_button_new_with_label("Choose JSON File");
    gtk_grid_attach(GTK_GRID(grid), choose_file_button, 0, 4, 2, 1);
    g_signal_connect(choose_file_button, "clicked", G_CALLBACK(choose_json_file), movie_data);

    // Show all widgets
    gtk_widget_show_all(window);

    // Run main GTK loop
    gtk_main();

    // Clean up allocated memory
    if (movie_data->json_file_path) {
        g_free(movie_data->json_file_path);
    }
    g_free(movie_data);

    return 0;
}
