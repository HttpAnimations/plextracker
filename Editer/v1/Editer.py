import json
import tkinter as tk
from tkinter import messagebox, filedialog

def load_data():
    try:
        with open('movies.json', 'r') as file:
            data = json.load(file)
        return data
    except FileNotFoundError:
        messagebox.showwarning("File Not Found", "movies.json not found. Creating new file.")
        data = {"movies": []}
        with open('movies.json', 'w') as file:
            json.dump(data, file, indent=2)
        return data
    except json.JSONDecodeError:
        messagebox.showerror("JSON Decode Error", "Error decoding movies.json. Check file format.")
        return None

def save_data(data):
    try:
        with open('movies.json', 'w') as file:
            json.dump(data, file, indent=2)
        messagebox.showinfo("Save Successful", "movies.json saved successfully.")
    except Exception as e:
        messagebox.showerror("Save Error", f"Error saving movies.json: {e}")

def add_movie():
    title = title_entry.get()
    icon = icon_entry.get()
    release_date = release_date_entry.get()

    if not title or not icon or not release_date:
        messagebox.showerror("Error", "All fields are required.")
        return

    new_movie = {
        "title": title,
        "icon": icon,
        "release_date": release_date
    }
    movies_listbox.insert(tk.END, title)
    movies_data["movies"].append(new_movie)
    save_data(movies_data)

def delete_movie():
    selected_index = movies_listbox.curselection()
    if selected_index:
        idx = selected_index[0]
        movie_title = movies_listbox.get(idx)
        confirm = messagebox.askyesno("Confirm Deletion", f"Are you sure you want to delete '{movie_title}'?")
        if confirm:
            del movies_data["movies"][idx]
            movies_listbox.delete(idx)
            save_data(movies_data)

# Initialize Tkinter app
app = tk.Tk()
app.title("Movies JSON Editor")

# Load or create movies.json
movies_data = load_data()

# GUI Elements
title_label = tk.Label(app, text="Title:")
title_label.grid(row=0, column=0, padx=10, pady=10)
title_entry = tk.Entry(app, width=50)
title_entry.grid(row=0, column=1, padx=10, pady=10)

icon_label = tk.Label(app, text="Icon URL:")
icon_label.grid(row=1, column=0, padx=10, pady=10)
icon_entry = tk.Entry(app, width=50)
icon_entry.grid(row=1, column=1, padx=10, pady=10)

release_date_label = tk.Label(app, text="Release Date:")
release_date_label.grid(row=2, column=0, padx=10, pady=10)
release_date_entry = tk.Entry(app, width=50)
release_date_entry.grid(row=2, column=1, padx=10, pady=10)

add_button = tk.Button(app, text="Add Movie", command=add_movie)
add_button.grid(row=3, column=0, columnspan=2, pady=10)

movies_listbox = tk.Listbox(app, width=60, height=10)
movies_listbox.grid(row=4, column=0, columnspan=2, padx=10, pady=10)

delete_button = tk.Button(app, text="Delete Selected", command=delete_movie)
delete_button.grid(row=5, column=0, columnspan=2, pady=10)

# Populate listbox with existing movies
if movies_data:
    for movie in movies_data["movies"]:
        movies_listbox.insert(tk.END, movie["title"])

# Start the Tkinter main loop
app.mainloop()
