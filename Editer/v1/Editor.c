#include <gtk/gtk.h>
#include <json-c/json.h>
#include <stdio.h>

// Global variables
json_object *movies_array;

// Function to load movies.json
void load_movies_json() {
    FILE *file = fopen("movies.json", "r");
    if (!file) {
        g_print("movies.json not found. Creating a new file.\n");
        movies_array = json_object_new_array();
    } else {
        fseek(file, 0, SEEK_END);
        long file_size = ftell(file);
        rewind(file);
        char *buffer = (char *)malloc(file_size + 1);
        fread(buffer, 1, file_size, file);
        buffer[file_size] = '\0';

        movies_array = json_tokener_parse(buffer);
        fclose(file);
    }
}

// Function to save movies.json
void save_movies_json() {
    FILE *file = fopen("movies.json", "w");
    if (!file) {
        g_print("Error opening movies.json for writing.\n");
        return;
    }

    const char *json_str = json_object_to_json_string_ext(movies_array, JSON_C_TO_STRING_PRETTY);
    fprintf(file, "%s\n", json_str);
    fclose(file);
}

// Callback function to add a movie
void add_movie(GtkWidget *widget, gpointer data) {
    const gchar *title = gtk_entry_get_text(GTK_ENTRY(data));
    // Here you can add validations for title, icon URL, and release date
    // For simplicity, assume they are provided correctly
    json_object *movie_obj = json_object_new_object();
    json_object_object_add(movie_obj, "title", json_object_new_string(title));

    json_object_array_add(movies_array, movie_obj);
    save_movies_json();

    g_print("Movie added: %s\n", title);
    // Clear the entry after adding
    gtk_entry_set_text(GTK_ENTRY(data), "");
}

// Callback function to delete a movie
void delete_movie(GtkWidget *widget, gpointer data) {
    GtkListBox *listbox = GTK_LIST_BOX(data);
    GtkListBoxRow *selected_row = gtk_list_box_get_selected_row(listbox);
    if (selected_row) {
        gint index = gtk_list_box_row_get_index(selected_row);
        json_object_array_del_idx(movies_array, index, 1);
        save_movies_json();

        g_print("Movie deleted at index: %d\n", index);
        gtk_container_remove(GTK_CONTAINER(listbox), GTK_WIDGET(selected_row));
    }
}

// Main function
int main(int argc, char *argv[]) {
    load_movies_json();

    // Initialize GTK
    gtk_init(&argc, &argv);

    // Create main window
    GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Movies JSON Editor");
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    // Vertical box layout
    GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_container_add(GTK_CONTAINER(window), vbox);

    // Entry for movie title
    GtkWidget *title_entry = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(title_entry), "Enter movie title");
    gtk_box_pack_start(GTK_BOX(vbox), title_entry, FALSE, FALSE, 0);

    // Add button
    GtkWidget *add_button = gtk_button_new_with_label("Add Movie");
    g_signal_connect(add_button, "clicked", G_CALLBACK(add_movie), title_entry);
    gtk_box_pack_start(GTK_BOX(vbox), add_button, FALSE, FALSE, 0);

    // Listbox to display movies
    GtkWidget *listbox = gtk_list_box_new();
    gtk_box_pack_start(GTK_BOX(vbox), listbox, TRUE, TRUE, 0);

    // Populate listbox with existing movies
    int array_len = json_object_array_length(movies_array);
    for (int i = 0; i < array_len; ++i) {
        json_object *movie_obj = json_object_array_get_idx(movies_array, i);
        const char *title = json_object_get_string(json_object_object_get(movie_obj, "title"));
        GtkWidget *row_label = gtk_label_new(title);
        gtk_list_box_insert(GTK_LIST_BOX(listbox), row_label, -1);
    }

    // Delete button
    GtkWidget *delete_button = gtk_button_new_with_label("Delete Movie");
    g_signal_connect(delete_button, "clicked", G_CALLBACK(delete_movie), listbox);
    gtk_box_pack_start(GTK_BOX(vbox), delete_button, FALSE, FALSE, 0);

    // Show all widgets
    gtk_widget_show_all(window);

    // Start GTK main loop
    gtk_main();

    return 0;
}
