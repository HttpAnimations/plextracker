#include <gtk/gtk.h>
#include <json-c/json.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Structure to hold movie information
typedef struct {
    GtkWidget *name_entry;
    GtkWidget *icon_entry;
    GtkWidget *year_entry;
    GtkWidget *movies_list;
} MovieData;

// Function to add a movie
void add_movie(GtkWidget *widget, gpointer data) {
    MovieData *movie_data = (MovieData *)data;

    const gchar *name = gtk_entry_get_text(GTK_ENTRY(movie_data->name_entry));
    const gchar *icon = gtk_entry_get_text(GTK_ENTRY(movie_data->icon_entry));
    const gchar *year = gtk_entry_get_text(GTK_ENTRY(movie_data->year_entry));

    // Validate input (you can add more validation as needed)
    if (strlen(name) == 0 || strlen(icon) == 0 || strlen(year) == 0) {
        g_print("Please fill in all fields.\n");
        return;
    }

    // Create JSON object for the new movie
    json_object *new_movie = json_object_new_object();
    json_object_object_add(new_movie, "title", json_object_new_string(name));
    json_object_object_add(new_movie, "icon", json_object_new_string(icon));
    json_object_object_add(new_movie, "release_date", json_object_new_string(year));

    // Add the new movie to the JSON array
    json_object *json_array = json_object_new_array();
    json_object_array_add(json_array, new_movie);

    // Convert JSON object to string and print it (you can save it to file instead)
    const char *json_string = json_object_to_json_string_ext(json_array, JSON_C_TO_STRING_PRETTY);
    g_print("%s\n", json_string);

    // Clean up JSON objects
    json_object_put(new_movie);
    json_object_put(json_array);

    // Clear entry fields after adding movie
    gtk_entry_set_text(GTK_ENTRY(movie_data->name_entry), "");
    gtk_entry_set_text(GTK_ENTRY(movie_data->icon_entry), "");
    gtk_entry_set_text(GTK_ENTRY(movie_data->year_entry), "");
}

// Main function
int main(int argc, char *argv[]) {
    GtkWidget *window;
    GtkWidget *grid;
    GtkWidget *add_movie_button;
    MovieData *movie_data;

    // Initialize GTK
    gtk_init(&argc, &argv);

    // Create main window
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Movie Editor");
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    // Create grid layout
    grid = gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(window), grid);

    // Create entries for movie data
    GtkWidget *name_label = gtk_label_new("Name:");
    gtk_grid_attach(GTK_GRID(grid), name_label, 0, 0, 1, 1);
    movie_data->name_entry = gtk_entry_new();
    gtk_grid_attach(GTK_GRID(grid), movie_data->name_entry, 1, 0, 1, 1);

    GtkWidget *icon_label = gtk_label_new("Icon Path:");
    gtk_grid_attach(GTK_GRID(grid), icon_label, 0, 1, 1, 1);
    movie_data->icon_entry = gtk_entry_new();
    gtk_grid_attach(GTK_GRID(grid), movie_data->icon_entry, 1, 1, 1, 1);

    GtkWidget *year_label = gtk_label_new("Year:");
    gtk_grid_attach(GTK_GRID(grid), year_label, 0, 2, 1, 1);
    movie_data->year_entry = gtk_entry_new();
    gtk_grid_attach(GTK_GRID(grid), movie_data->year_entry, 1, 2, 1, 1);

    // Create button to add movie
    add_movie_button = gtk_button_new_with_label("Add Movie");
    gtk_grid_attach(GTK_GRID(grid), add_movie_button, 0, 3, 2, 1);
    g_signal_connect(add_movie_button, "clicked", G_CALLBACK(add_movie), movie_data);

    // Show all widgets
    gtk_widget_show_all(window);

    // Run main GTK loop
    gtk_main();

    return 0;
}
