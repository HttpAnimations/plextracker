#include <gtk/gtk.h>
#include <json-c/json.h>

json_object *movies_array = NULL;
const gchar *json_file_path = NULL;

void load_movies_json(const gchar *file_path) {
    FILE *file = fopen(file_path, "r");
    if (!file) {
        g_print("movies.json not found. Creating a new file.\n");
        movies_array = json_object_new_array();  // Initialize as new array
    } else {
        fseek(file, 0, SEEK_END);
        long file_size = ftell(file);
        rewind(file);
        char *buffer = (char *)malloc(file_size + 1);
        fread(buffer, 1, file_size, file);
        buffer[file_size] = '\0';

        movies_array = json_tokener_parse(buffer);  // Parse existing file
        fclose(file);
    }
    json_file_path = file_path;
}

void save_movies_json() {
    if (!json_file_path || !movies_array) {
        g_print("Error: Unable to save movies.json. File path or array not initialized.\n");
        return;
    }

    const char *json_str = json_object_to_json_string_ext(movies_array, JSON_C_TO_STRING_PRETTY);
    FILE *file = fopen(json_file_path, "w");
    if (!file) {
        g_print("Error: Unable to open movies.json for writing.\n");
        return;
    }
    fputs(json_str, file);
    fclose(file);
}

void add_movie(GtkWidget *widget, gpointer data[]) {
    const gchar *title = gtk_entry_get_text(GTK_ENTRY(data[0]));
    const gchar *icon_path = gtk_entry_get_text(GTK_ENTRY(data[1]));
    const gchar *year = gtk_entry_get_text(GTK_ENTRY(data[2]));

    // Validate inputs
    if (strlen(title) == 0 || strlen(icon_path) == 0 || strlen(year) == 0) {
        g_print("Please fill in all fields.\n");
        return;
    }

    // Ensure movies_array is initialized as a JSON array
    if (!movies_array || !json_object_is_type(movies_array, json_type_array)) {
        g_print("Error: movies_array is not initialized as a JSON array.\n");
        return;
    }

    // Create movie object
    json_object *movie_obj = json_object_new_object();
    json_object_object_add(movie_obj, "title", json_object_new_string(title));
    json_object_object_add(movie_obj, "icon", json_object_new_string(icon_path));
    json_object_object_add(movie_obj, "release_date", json_object_new_string(year));

    // Add movie to array and save
    json_object_array_add(movies_array, movie_obj);
    save_movies_json();
    g_print("Movie added: %s\n", title);

    // Clear entry fields after adding
    gtk_entry_set_text(GTK_ENTRY(data[0]), "");
    gtk_entry_set_text(GTK_ENTRY(data[1]), "");
    gtk_entry_set_text(GTK_ENTRY(data[2]), "");

    // Update right panel with added movie (if needed)
    // Example: refresh_movie_listbox();
}

int main(int argc, char *argv[]) {
    GtkWidget *window;
    GtkWidget *grid;
    GtkWidget *sidebar;
    GtkWidget *content;
    GtkWidget *add_movie_button;
    GtkWidget *movie_name_entry;
    GtkWidget *movie_icon_entry;
    GtkWidget *movie_year_entry;
    GtkWidget *movies_listbox;

    gtk_init(&argc, &argv);

    // Create window
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Movie Editor");
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 600);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    // Create grid container
    grid = gtk_grid_new();
    gtk_container_add(GTK_CONTAINER(window), grid);

    // Create sidebar
    sidebar = gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), sidebar, 0, 0, 1, 1);

    // Add movie inputs
    movie_name_entry = gtk_entry_new();
    gtk_grid_attach(GTK_GRID(sidebar), gtk_label_new("Name:"), 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(sidebar), movie_name_entry, 1, 0, 1, 1);

    movie_icon_entry = gtk_entry_new();
    gtk_grid_attach(GTK_GRID(sidebar), gtk_label_new("Icon Path:"), 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(sidebar), movie_icon_entry, 1, 1, 1, 1);

    movie_year_entry = gtk_entry_new();
    gtk_grid_attach(GTK_GRID(sidebar), gtk_label_new("Year:"), 0, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(sidebar), movie_year_entry, 1, 2, 1, 1);

    // Add "Add Movie" button
    add_movie_button = gtk_button_new_with_label("Add Movie");
    g_signal_connect(add_movie_button, "clicked", G_CALLBACK(add_movie), (gpointer) (GtkWidget*[3]){movie_name_entry, movie_icon_entry, movie_year_entry});
    gtk_grid_attach(GTK_GRID(sidebar), add_movie_button, 0, 3, 2, 1);

    // Create content area
    content = gtk_grid_new();
    gtk_grid_attach(GTK_GRID(grid), content, 1, 0, 1, 1);

    // Add movies listbox
    movies_listbox = gtk_list_box_new();
    gtk_grid_attach(GTK_GRID(content), movies_listbox, 0, 0, 1, 1);

    // Load movies.json
    const gchar *file_path = "/path/to/movies.json";  // Update with your file path
    load_movies_json(file_path);

    // Display existing movies in movies_listbox
    if (movies_array && json_object_is_type(movies_array, json_type_array)) {
        int array_len = json_object_array_length(movies_array);
        for (int i = 0; i < array_len; i++) {
            json_object *movie_obj = json_object_array_get_idx(movies_array, i);
            const gchar *title = json_object_get_string(json_object_object_get(movie_obj, "title"));
            const gchar *icon_path = json_object_get_string(json_object_object_get(movie_obj, "icon"));
            const gchar *year = json_object_get_string(json_object_object_get(movie_obj, "release_date"));

            GtkWidget *movie_box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
            GtkWidget *movie_icon = gtk_image_new_from_file(icon_path);
            GtkWidget *movie_label = gtk_label_new(NULL);
            gtk_label_set_markup(GTK_LABEL(movie_label), g_strdup_printf("<b>%s</b>\n%s", title, year));
            gtk_box_pack_start(GTK_BOX(movie_box), movie_icon, FALSE, FALSE, 0);
            gtk_box_pack_start(GTK_BOX(movie_box), movie_label, FALSE, FALSE, 0);
            gtk_container_add(GTK_CONTAINER(movies_listbox), movie_box);
        }
    }

    // Show all widgets
    gtk_widget_show_all(window);

    // Start GTK main loop
    gtk_main();

    return 0;
}
