#include <gtk/gtk.h>
#include <json-c/json.h>
#include <stdio.h>

// Global variables
json_object *movies_array;
const gchar *json_file_path = NULL;

// Function to load movies.json from chosen path
void load_movies_json(const gchar *file_path) {
    FILE *file = fopen(file_path, "r");
    if (!file) {
        g_print("movies.json not found. Creating a new file.\n");
        movies_array = json_object_new_array();
    } else {
        fseek(file, 0, SEEK_END);
        long file_size = ftell(file);
        rewind(file);
        char *buffer = (char *)malloc(file_size + 1);
        fread(buffer, 1, file_size, file);
        buffer[file_size] = '\0';

        movies_array = json_tokener_parse(buffer);
        fclose(file);
    }
    json_file_path = file_path;
}

// Function to save movies.json
void save_movies_json() {
    if (!json_file_path) {
        g_print("No file path selected. Cannot save.\n");
        return;
    }

    FILE *file = fopen(json_file_path, "w");
    if (!file) {
        g_print("Error opening movies.json for writing.\n");
        return;
    }

    const char *json_str = json_object_to_json_string_ext(movies_array, JSON_C_TO_STRING_PRETTY);
    fprintf(file, "%s\n", json_str);
    fclose(file);
}

// Callback function to add a movie
void add_movie(GtkWidget *widget, gpointer data[]) {
    const gchar *title = gtk_entry_get_text(GTK_ENTRY(data[0]));
    const gchar *icon_path = gtk_entry_get_text(GTK_ENTRY(data[1]));
    const gchar *year = gtk_entry_get_text(GTK_ENTRY(data[2]));

    // Validate inputs
    if (strlen(title) == 0 || strlen(icon_path) == 0 || strlen(year) == 0) {
        g_print("Please fill in all fields.\n");
        return;
    }

    // Create movie object
    json_object *movie_obj = json_object_new_object();
    json_object_object_add(movie_obj, "title", json_object_new_string(title));
    json_object_object_add(movie_obj, "icon", json_object_new_string(icon_path));
    json_object_object_add(movie_obj, "release_date", json_object_new_string(year));

    // Add movie to array and save
    json_object_array_add(movies_array, movie_obj);
    save_movies_json();

    g_print("Movie added: %s\n", title);

    // Clear entry fields after adding
    gtk_entry_set_text(GTK_ENTRY(data[0]), "");
    gtk_entry_set_text(GTK_ENTRY(data[1]), "");
    gtk_entry_set_text(GTK_ENTRY(data[2]), "");
}

// Callback function to delete a movie
void delete_movie(GtkWidget *widget, gpointer data) {
    GtkListBox *listbox = GTK_LIST_BOX(data);
    GtkListBoxRow *selected_row = gtk_list_box_get_selected_row(listbox);
    if (selected_row) {
        gint index = gtk_list_box_row_get_index(selected_row);
        json_object_array_del_idx(movies_array, index, 1);
        save_movies_json();

        g_print("Movie deleted at index: %d\n", index);
        gtk_container_remove(GTK_CONTAINER(listbox), GTK_WIDGET(selected_row));
    }
}

// Callback function to handle file chooser dialog
void choose_file(GtkWidget *widget, gpointer window) {
    GtkWidget *dialog = gtk_file_chooser_dialog_new("Select movies.json file",
                                                    GTK_WINDOW(window),
                                                    GTK_FILE_CHOOSER_ACTION_OPEN,
                                                    "_Cancel", GTK_RESPONSE_CANCEL,
                                                    "_Open", GTK_RESPONSE_ACCEPT,
                                                    NULL);

    GtkFileChooser *chooser = GTK_FILE_CHOOSER(dialog);
    gtk_file_chooser_set_current_folder(chooser, g_get_home_dir());

    gint res = gtk_dialog_run(GTK_DIALOG(dialog));
    if (res == GTK_RESPONSE_ACCEPT) {
        char *filename = gtk_file_chooser_get_filename(chooser);
        load_movies_json(filename);
        g_free(filename);
    }

    gtk_widget_destroy(dialog);
}

// Main function
int main(int argc, char *argv[]) {
    // Initialize GTK
    gtk_init(&argc, &argv);

    // Create main window
    GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Movies JSON Editor");
    gtk_window_set_default_size(GTK_WINDOW(window), 800, 400);
    gtk_container_set_border_width(GTK_CONTAINER(window), 10);
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    // Vertical box layout
    GtkWidget *vbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_container_add(GTK_CONTAINER(window), vbox);

    // Left panel (Add Movie)
    GtkWidget *left_panel = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_pack_start(GTK_BOX(vbox), left_panel, TRUE, TRUE, 0);

    GtkWidget *title_entry = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(title_entry), "Movie Title");
    gtk_box_pack_start(GTK_BOX(left_panel), title_entry, FALSE, FALSE, 0);

    GtkWidget *icon_entry = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(icon_entry), "Icon Path");
    gtk_box_pack_start(GTK_BOX(left_panel), icon_entry, FALSE, FALSE, 0);

    GtkWidget *year_entry = gtk_entry_new();
    gtk_entry_set_placeholder_text(GTK_ENTRY(year_entry), "Release Year");
    gtk_box_pack_start(GTK_BOX(left_panel), year_entry, FALSE, FALSE, 0);

    GtkWidget *add_button = gtk_button_new_with_label("Add Movie");
    gtk_box_pack_start(GTK_BOX(left_panel), add_button, FALSE, FALSE, 0);

    // Connect add_movie function with add_button click event
    gpointer data[] = {title_entry, icon_entry, year_entry};
    g_signal_connect(add_button, "clicked", G_CALLBACK(add_movie), data);

    // Right panel (List of Movies)
    GtkWidget *right_panel = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_pack_start(GTK_BOX(vbox), right_panel, TRUE, TRUE, 0);

    GtkWidget *listbox = gtk_list_box_new();
    gtk_box_pack_start(GTK_BOX(right_panel), listbox, TRUE, TRUE, 0);

    GtkWidget *delete_button = gtk_button_new_with_label("Delete Movie");
    gtk_box_pack_start(GTK_BOX(right_panel), delete_button, FALSE, FALSE, 0);

    // Connect delete_movie function with delete_button click event
    g_signal_connect(delete_button, "clicked", G_CALLBACK(delete_movie), listbox);

    // File chooser button
    GtkWidget *file_button = gtk_button_new_with_label("Choose movies.json File");
    g_signal_connect(file_button, "clicked", G_CALLBACK(choose_file), window);
    gtk_box_pack_start(GTK_BOX(left_panel), file_button, FALSE, FALSE, 0);

    // Show all widgets
    gtk_widget_show_all(window);

    // Start GTK main loop
    gtk_main();

    return 0;
}
