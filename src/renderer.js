const { ipcRenderer } = require('electron');
const storage = require('electron-json-storage').getDefaultDataPath();

document.getElementById('moviesBtn').addEventListener('click', () => {
  // Handle Movies category selection
});

document.getElementById('tvShowsBtn').addEventListener('click', () => {
  // Handle TV Shows category selection
});

document.getElementById('addItemBtn').addEventListener('click', () => {
  const title = document.getElementById('titleInput').value;
  const year = document.getElementById('yearInput').value;

  if (title && year) {
    const item = { title, year };
    ipcRenderer.send('add-item', item);
    document.getElementById('titleInput').value = '';
    document.getElementById('yearInput').value = '';
    displayItems(); // Update item list
  }
});

function displayItems() {
  // Fetch and display items from JSON storage
}
