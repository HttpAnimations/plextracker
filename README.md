# PlexTracker

An personal tracker for my Plex server


# Editer

Install lua-lgi
```bash
sudo pacman -S lua-lgi
```

Install LuaRocks
```bash
sudo pacman -S luarocks
```

Install Lua JSON Module
```bash
sudo luarocks install lua-json
```


Run
```bash
lua Edit.lua
```