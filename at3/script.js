// script.js
document.addEventListener('DOMContentLoaded', function() {
    const { ipcRenderer } = window.electron; // Access ipcRenderer from exposed context in preload.js

    // Form submission to add new movie
    const addMovieForm = document.getElementById('add-movie-form');
    addMovieForm.addEventListener('submit', function(event) {
        event.preventDefault();

        const title = document.getElementById('movie-title').value;
        const year = document.getElementById('movie-year').value;

        // Ensure ipcRenderer is properly initialized
        if (ipcRenderer) {
            // Send movie data to main process
            ipcRenderer.send('add-movie', { title, year });

            // Clear form inputs
            document.getElementById('movie-title').value = '';
            document.getElementById('movie-year').value = '';
        } else {
            console.error('ipcRenderer is not available');
        }
    });
});
