local lgi = require("lgi")
local Gtk = lgi.require("Gtk", "3.0")
local Gio = lgi.require("Gio", "2.0")
local Json = require("json")

-- Function to save movies to JSON file
local function save_movies(movies, file_path)
    local json_data = Json.encode_pretty({ movies = movies })
    local file = Gio.File.new_for_path(file_path)
    local success, err = file:set_contents(json_data)
    if not success then
        print("Error saving file:", err)
    end
end

-- Function to load movies from JSON file
local function load_movies(file_path)
    local file = Gio.File.new_for_path(file_path)
    local success, data, _ = file:load_contents()
    if success then
        local decoded_data = Json.decode(data)
        return decoded_data.movies or {}
    else
        print("Error loading file:", data)
        return {}
    end
end

local function add_movie(button, movie_data)
    local name = movie_name_entry:get_text()
    local icon = movie_icon_entry:get_text()
    local year = movie_year_entry:get_text()

    if name == "" or icon == "" or year == "" then
        print("Please fill in all fields.")
        return
    end

    local new_movie = {
        title = name,
        icon = icon,
        release_date = year
    }

    table.insert(movie_data, new_movie)
    save_movies(movie_data, "json/movies.json")
    load_movies_list(movie_data)

    movie_name_entry:set_text("")
    movie_icon_entry:set_text("")
    movie_year_entry:set_text("")
end

local function load_movies_list(movie_data)
    movie_list_store:clear()
    for _, movie in ipairs(movie_data) do
        local iter = movie_list_store:append()
        movie_list_store:set(iter, { 1, movie.title, 2, movie.icon, 3, movie.release_date })
    end
end

-- Create the main window
local app = Gtk.Application {
    application_id = "com.example.PlexTracker"
}

function app:on_activate()
    local window = Gtk.ApplicationWindow {
        application = app,
        title = "Plex Tracker - Movies",
        default_width = 800,
        default_height = 600,
        on_destroy = Gtk.main_quit
    }

    -- Create a grid layout
    local grid = Gtk.Grid {
        column_homogeneous = true,
        row_spacing = 5,
        column_spacing = 10,
        margin = 10
    }

    -- Left panel for input fields
    local left_panel = Gtk.Box {
        orientation = Gtk.Orientation.VERTICAL,
        spacing = 10
    }

    local name_label = Gtk.Label {
        label = "Name:"
    }

    movie_name_entry = Gtk.Entry {
        placeholder_text = "Enter movie name"
    }

    local icon_label = Gtk.Label {
        label = "Icon Path:"
    }

    movie_icon_entry = Gtk.Entry {
        placeholder_text = "Enter icon path"
    }

    local year_label = Gtk.Label {
        label = "Release Year:"
    }

    movie_year_entry = Gtk.Entry {
        placeholder_text = "Enter release year"
    }

    left_panel:pack_start(name_label, false, false, 0)
    left_panel:pack_start(movie_name_entry, false, false, 0)
    left_panel:pack_start(icon_label, false, false, 0)
    left_panel:pack_start(movie_icon_entry, false, false, 0)
    left_panel:pack_start(year_label, false, false, 0)
    left_panel:pack_start(movie_year_entry, false, false, 0)

    -- Button to add movie
    local add_button = Gtk.Button {
        label = "Add Movie"
    }
    add_button.on_clicked = function()
        add_movie(add_button, movie_data)
    end

    left_panel:pack_start(add_button, false, false, 0)

    -- Right panel for displaying movies
    local right_panel = Gtk.Box {
        orientation = Gtk.Orientation.VERTICAL,
        spacing = 10
    }

    local movie_list_store = Gtk.ListStore.new { Gtk.Type.STRING, Gtk.Type.STRING, Gtk.Type.STRING }
    local tree_view = Gtk.TreeView {
        model = movie_list_store,
        headers_visible = true,
        hexpand = true,
        vexpand = true
    }

    local renderer = Gtk.CellRendererText {}

    local column1 = Gtk.TreeViewColumn {
        title = "Name",
        { renderer, { text = 2 } }
    }
    tree_view:append_column(column1)

    local column2 = Gtk.TreeViewColumn {
        title = "Icon Path",
        { renderer, { text = 3 } }
    }
    tree_view:append_column(column2)

    local column3 = Gtk.TreeViewColumn {
        title = "Year",
        { renderer, { text = 4 } }
    }
    tree_view:append_column(column3)

    right_panel:pack_start(tree_view, true, true, 0)

    -- Add left and right panels to the grid layout
    grid:attach(left_panel, 0, 0, 1, 1)
    grid:attach(right_panel, 1, 0, 1, 1)

    -- Add grid to the window and show everything
    window:add(grid)
    window:show_all()
end

app:run { arg[0], ... }
